
/*
 * @brief Sample hello world program with initialisation code based on NXP examples
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include <chip.h>
#include <stdarg.h>
#include <stdio.h>

#if HAVE_FREERTOS
#include <FreeRTOS.h>
#include <task.h>
#endif

#if HAVE_FREERTOS
uint32_t SystemCoreClock;
#else
volatile uint32_t ticks;
#endif

#if HAVE_SEMIHOST

#define DEBUG(...) printf(__VA_ARGS__)

#else

#if (defined(CHIP_LPC175X_6X) || defined(CHIP_LPC177X_8X))
// so much for having the same peripheral access API across the chip families

#define LPC_USART LPC_UART0
#define UART_IRQHandler UART0_IRQHandler

#define Chip_UART_ReadLineStatus(U) Chip_UART_GetLineStatus(U)

static inline uint8_t Chip_UART_ReadByte(LPC_USART_T *u) {
  uint8_t c;
  Chip_UART_ReceiveByte(LPC_USART, &c);
  return c;
}
#endif

void outc(char c) {

  while(!(Chip_UART_ReadLineStatus(LPC_USART) & UART_LSR_THRE)) {
    // relax
  }
  Chip_UART_SendByte(LPC_USART, (uint8_t)c);
}

void outs(char *s) {
  while(*s)
    outc(*s++);
}

void DEBUG(const char *f, ...) __attribute__ ((format (printf, 1, 2)));

void DEBUG(const char *f, ...) {
  va_list ap;
  static char buf[128];
  va_start(ap, f);
  vsnprintf(buf, sizeof(buf), f, ap);
  va_end(ap);
  outs(buf);
}

#endif // !HAVE_SEMIHOST

#if defined(CHIP_LPC1347)

void SystemSetupClocking(void)
{
	int i;

	/* Powerup main oscillator */
	Chip_SYSCTL_PowerUp(SYSCTL_POWERDOWN_SYSOSC_PD);

	/* Wait an estimated 200us for OSC to be stablized, no status
	   indication, dummy wait */
	for (i = 0; i < 0x100; i++) {}

	/* Set system PLL input to main oscillator */
	Chip_Clock_SetSystemPllSource(SYSCTL_PLLCLKSRC_SYSOSC);

	/* Powerup system PLL */
	Chip_SYSCTL_PowerUp(SYSCTL_POWERDOWN_SYSPLL_PD);

	/* Setup PLL for main oscillator rate (FCLKIN = 12MHz) * 6 = 72MHz
	   MSEL = 5 (this is pre-decremented), PSEL = 1 (for P = 2)
	   FCLKOUT = FCLKIN * (MSEL + 1) = 12MHz * 6 = 72MHz
	   FCCO = FCLKOUT * 2 * P = 72MHz * 2 * 2 = 288MHz (within FCCO range) */
	Chip_Clock_SetupSystemPLL(5, 1);

	/* Wait for PLL to lock */
	while (!Chip_Clock_IsSystemPLLLocked()) {}

	/* Set system clock divider to 1 */
	Chip_Clock_SetSysClockDiv(1);

	/* Setup FLASH access to 3 clocks (72MHz clock) */
	Chip_FMC_SetFLASHAccess(FLASHTIM_72MHZ_CPU);

	/* Set main clock source to the system PLL. This will drive 72MHz
	   for the main clock and 72MHz for the system clock */
	Chip_Clock_SetMainClockSource(SYSCTL_MAINCLKSRC_PLLOUT);

	/* Enable IOCON clock */
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);
}

#endif // defined(CHIP_LPC1347)

#if defined(CHIP_LPC11UXX) || defined(CHIP_LPC1343)

void SystemSetupClocking(void) {

  /* Set system PLL input to IRC */
  Chip_Clock_SetSystemPllSource(SYSCTL_PLLCLKSRC_IRC);

  /* Powerup system PLL */
  Chip_SYSCTL_PowerUp(SYSCTL_POWERDOWN_SYSPLL_PD);

  /* Setup PLL for main oscillator rate (FCLKIN = 12MHz) * 6 = 72MHz
  MSEL = 5 (this is pre-decremented), PSEL = 1 (for P = 2)
  FCLKOUT = FCLKIN * (MSEL + 1) = 12MHz * 6 = 72MHz
  FCCO = FCLKOUT * 2 * P = 72MHz * 2 * 2 = 288MHz (within FCCO range) */
  Chip_Clock_SetupSystemPLL(5, 1);

#if 1
  /* Wait for PLL to lock */
  while (!Chip_Clock_IsSystemPLLLocked()) {}

  /* Set system clock divider to 1 */
  Chip_Clock_SetSysClockDiv(1);

  /* Setup FLASH access to 3 clocks (72MHz clock) */
  Chip_FMC_SetFLASHAccess(2);

  /* Set main clock source to the system PLL. This will drive 72MHz
  for the main clock and 72MHz for the system clock */
  Chip_Clock_SetMainClockSource(SYSCTL_MAINCLKSRC_PLLOUT);
#endif

  /* Enable IOCON clock */
  Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);
}

#endif

#if defined(CHIP_LPC11UXX) && !HAVE_SEMIHOST

typedef struct {
  uint8_t port;
  uint8_t pin;
  uint16_t mode;
  uint8_t func;
} PINMUX_GRP_T;

/* Pin muxing table, only items that need changing from their default pin state are in this table. */
STATIC const PINMUX_GRP_T pinmuxing[] = {
  { 1, 14,  MD_DIGMODE|MD_PLN, FUNC3 }, // digital, pull-up, RXD
  { 1, 13,  MD_DIGMODE, FUNC3 }, // digital, TXD
};

void SystemSetupMuxing(void) {
  for(int i = 0; i < (sizeof(pinmuxing) / sizeof(PINMUX_GRP_T)); i++) {
    Chip_IOCON_PinMux(LPC_IOCON, pinmuxing[i].port, pinmuxing[i].pin, pinmuxing[i].mode, pinmuxing[i].func);
  }
}

#endif

#if defined(CHIP_LPC1343) && !HAVE_SEMIHOST

typedef struct {
  uint8_t pin;	 // Pin number
  uint16_t mode; // Pin mode
  uint8_t func;	 // Function number
} PINMUX_GRP_T;

/* Pin muxing table, only items that need changing from their default pin state are in this table. */
STATIC const PINMUX_GRP_T pinmuxing[] = {
//  { IOCON_PIO0_11, MD_ADMODE, FUNC2 }, // analog, AD0
//  { IOCON_PIO1_3,  MD_ADMODE, FUNC2 }, // analog, AD4
//  { IOCON_PIO1_10, MD_ADMODE, FUNC1 }, // analog, AD6
//  { IOCON_PIO1_11, MD_ADMODE, FUNC1 }, // analog, AD7
  { IOCON_PIO1_6,  MD_DIGMODE|MD_PLN, FUNC1 }, // digital, pull-up, RXD
  { IOCON_PIO1_7,  MD_DIGMODE, FUNC1 }, // digital, TXD
};

void SystemSetupMuxing(void) {
  for(int i = 0; i < (sizeof(pinmuxing) / sizeof(PINMUX_GRP_T)); i++) {
    Chip_IOCON_PinMux(LPC_IOCON, pinmuxing[i].pin, pinmuxing[i].mode, pinmuxing[i].func);
  }
}

#endif

#if defined(CHIP_LPC175X_6X) || defined(CHIP_LPC177X_8X)

void SystemSetupClocking(void) {

  Chip_Clock_SetMainPllSource(SYSCTL_PLLCLKSRC_IRC);
  Chip_Clock_SetCPUClockSource(SYSCTL_CCLKSRC_SYSCLK);

  /* Enable main oscillator used for PLLs */
  LPC_SYSCTL->SCS = SYSCTL_OSCEC;
  while ((LPC_SYSCTL->SCS & SYSCTL_OSCSTAT) == 0) {}

  /* PLL0 clock source is 12MHz oscillator */
  Chip_Clock_SetMainPllSource(SYSCTL_PLLCLKSRC_MAINOSC);

#if defined(CHIP_LPC175X_6X)
  /* Setup PLL0 for a 100MHz clock
  Input clock rate (FIN) is main oscillator = 12MHz */
  Chip_Clock_SetupPLL(SYSCTL_MAIN_PLL, 99, 5); // multiply by 99, divide by 6
#else
  /* Setup PLL0 for a 96MHz clock
  Input clock rate (FIN) is main oscillator = 12MHz */
  Chip_Clock_SetupPLL(SYSCTL_MAIN_PLL, 7, 0); // multiply by 8, divide by 1
#endif

  /* Enable PLL0 */
  Chip_Clock_EnablePLL(SYSCTL_MAIN_PLL, SYSCTL_PLL_ENABLE);

  /* Wait for main (CPU) PLL0 to lock */
  while (!Chip_Clock_IsMainPLLLocked()) {
    // relax
  }

  /* The CPU is still sourced from the SYSCLK, so set the CPU divider to 4 and switch it to the PLL0 clock */
  Chip_Clock_SetCPUClockDiv(3);
  Chip_Clock_SetCPUClockSource(SYSCTL_CCLKSRC_MAINPLL);

  Chip_FMC_SetFLASHAccess(FLASHTIM_80MHZ_CPU);
}

#endif

#if (defined(CHIP_LPC175X_6X) || defined(CHIP_LPC177X_8X)) && !HAVE_SEMIHOST

void SystemSetupMuxing(void) {
  Chip_IOCON_PinMux(LPC_IOCON, 0, 3, 0, FUNC1); // RXDO
  Chip_IOCON_PinMux(LPC_IOCON, 0, 2, 0, FUNC1); // TXDO
}

#endif

#if defined(CHIP_LPC1347) && !HAVE_SEMIHOST

void SystemSetupMuxing(void) {
  Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 18, (IOCON_FUNC1 | IOCON_MODE_INACT));	/* PIO0_18 used for RXD */
  Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 19, (IOCON_FUNC1 | IOCON_MODE_INACT));	/* PIO0_19 used for TXD */
}

#endif

void SystemInit(void) {
  // remember - bss is cleared after this function returns
  SystemSetupClocking();
#if !HAVE_SEMIHOST
  SystemSetupMuxing();
#endif
}

#if !HAVE_SEMIHOST

void UART_IRQHandler(void) {
  if(Chip_UART_ReadLineStatus(LPC_USART) & UART_LSR_RDR) {
    uint8_t c = Chip_UART_ReadByte(LPC_USART);
    outc(c);
  }
}

#endif

#if HAVE_FREERTOS

static portTASK_FUNCTION(helloWorldTask, pvParameters) {
  int n = 0;
  for(;;) {
    DEBUG("Hello world %d\n", ++n);
    vTaskDelay(configTICK_RATE_HZ);
  }
}

#else

void SysTick_Handler(void) {
  ++ticks;
  if(!(ticks % 1000)) {
    DEBUG("Hello world\n");
  }
}

#endif

int main(void) {
#if !HAVE_SEMIHOST
//  Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_UART0, SYSCTL_CLKDIV_8);
  Chip_UART_Init(LPC_USART);
  Chip_UART_SetBaud(LPC_USART, 115200);
#if defined(CHIP_LPC175X_6X) || defined(CHIP_LPC177X_8X)
  Chip_UART_ConfigData(LPC_USART, UART_DATABIT_8, UART_PARITY_NONE, UART_STOPBIT_1);
  UART_FIFO_CFG_T UARTFIFOConfigStruct;
  Chip_UART_FIFOConfigStructInit(LPC_USART, &UARTFIFOConfigStruct);
  UARTFIFOConfigStruct.FIFO_Level = UART_FIFO_TRGLEV2;
  Chip_UART_FIFOConfig(LPC_USART, &UARTFIFOConfigStruct);
  Chip_UART_TxCmd(LPC_USART, ENABLE);
  Chip_UART_IntConfig(LPC_USART, UART_INTCFG_RBR, ENABLE);
#else
  Chip_UART_ConfigData(LPC_USART, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
  Chip_UART_SetupFIFOS(LPC_USART, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
  Chip_UART_TXEnable(LPC_USART);
  Chip_UART_IntEnable(LPC_USART, UART_IER_RBRINT);
#endif
  NVIC_SetPriority(UART0_IRQn, 1);
  NVIC_EnableIRQ(UART0_IRQn);
#endif // !HAVE_SEMIHOST

#if HAVE_FREERTOS
  SystemCoreClock = Chip_Clock_GetSystemClockRate();
#else
  SysTick_Config(Chip_Clock_GetSystemClockRate() / 1000);
#endif

#if defined(CHIP_LPC175X_6X) || defined(CHIP_LPC177X_8X)
  DEBUG("sysclk      = %lu MHz\n", Chip_Clock_GetSystemClockRate() / 1000000);
  DEBUG("mainclk     = %lu MHz\n", Chip_Clock_GetMainClockRate() / 1000000);
  DEBUG("pllin       = %lu MHz\n", Chip_Clock_GetMainPllInClockRate() / 1000000);
  DEBUG("pllout      = %lu MHz\n", Chip_Clock_GetMainPllOutClockRate() / 1000000);
#endif
#if defined(CHIP_LPC175X_6X)
  DEBUG("uartclk     = %lu MHz\n", Chip_Clock_GetPeripheralClockRate(SYSCTL_PCLK_UART0) / 1000000);
  DEBUG("uartpclkdiv = %lu\n", Chip_Clock_GetPCLKDiv(SYSCTL_PCLK_UART0));
#endif

#if defined(CHIP_LPC1343) || defined(CHIP_LPC1347)
  DEBUG("PLL clock source = %lu\n", LPC_SYSCTL->SYSPLLCLKSEL);
  DEBUG("main clock source = %lu\n", LPC_SYSCTL->MAINCLKSEL);
  DEBUG("systemclk = %lu MHz\n", Chip_Clock_GetSystemClockRate() / 1000000);
  DEBUG("mainclk   = %lu MHz\n", Chip_Clock_GetMainClockRate() / 1000000);
  DEBUG("pllin     = %lu MHz\n", Chip_Clock_GetSystemPllInClockRate() / 1000000);
  DEBUG("pllout    = %lu MHz\n", Chip_Clock_GetSystemPllOutClockRate() / 1000000);
#endif

#if HAVE_FREERTOS
  portBASE_TYPE res = xTaskCreate(helloWorldTask, (signed char *) "HelloWorld", configMINIMAL_STACK_SIZE, NULL,
    (tskIDLE_PRIORITY + 1UL), (xTaskHandle *) NULL);
  if(res == pdPASS) {
    DEBUG("RTOS task scheduler starting...\n");
    vTaskStartScheduler();
    DEBUG("RTOS task scheduler exited\n");
  }
  else
    DEBUG("Failed to create task: %ld\n", res);
#else
  for(;;) {
    __WFI();
  }
#endif

  return 0;
}
