
SOURCE_PATH ?= .
VPATH ?= $(SOURCE_PATH):$(LPCOPEN_MAKE_DIR)
PROG ?= undefined
SOURCES ?= $(PROG).c
DEFAULT ?= $(PROG).hex

CHIP_FAMILY ?= 11xx
HAVE_FREERTOS ?= 0
HAVE_SEMIHOST ?= 0
STACK_SIZE ?= 0x200
HEAP_SIZE ?= 0x400
EXTRA_CFLAGS ?= -Wall -Os -std=gnu99 -fno-strict-aliasing
DEBUG_CFLAGS ?= -g
LD_SCRIPT ?= gcc_arm_$(FLASHK)k_$(RAMK)k.ld
BIN_SIZE ?= $(shell echo $$(($(FLASHK) * 1024)))
CORE = CORE_$(MX)
ARM_MATH = ARM_MATH_C$(MX)
STARTUP ?= startup_ARMC$(MX)
ARM_MATH_LIBS ?= $(CMSIS_MATH_LIB_DIR)/libarm_cortex$(MX)l_math.a
LIBS ?= $(LPCOPEN_LIBS) $(ARM_MATH_LIBS) -lc -lm $(EXTRA_LIBS)

all : $(DEFAULT)

LPCOPEN_MAKE_DIR ?= .
LPCOPEN_DIR ?= ./lpcopen
CMSIS_DIR ?= $(LPCOPEN_DIR)/software/CMSIS
CMSIS_MATH_LIB_DIR ?= $(CMSIS_DIR)/CMSIS/Lib/GCC
LPCOPEN_MAKEFILES ?= $(LPCOPEN_MAKE_DIR)/*.mk $(wildcard Makefile)

GCC_DIR ?= /opt/arm-none-eabi
GCC_PREFIX ?= arm-none-eabi-
GCC ?= $(GCC_DIR)/bin/$(GCC_PREFIX)gcc
GXX ?= $(GCC_DIR)/bin/$(GCC_PREFIX)g++
OBJCOPY ?= $(GCC_DIR)/bin/$(GCC_PREFIX)objcopy
SPECS ?= --specs=nano.specs $(PRINTF_FLOAT)
#PRINTF_FLOAT ?= -u _printf_float

ifeq ($(HAVE_SEMIHOST),1)
SPECS += --specs=rdimon.specs
EXTRA_LIBS += -lrdimon
endif

ifeq ($(CHIP_FAMILY),11xx)
ARCH = armv6-m
CPU = cortex-m0
MX = M0
CHIP ?= CHIP_LPC11UXX
FLASHK ?= 32
RAMK ?= 4
CORE_CHIP_OBJS = acmp_11xx.o adc_11xx.o clock_11xx.o gpio_11xx.o i2c_11xx.o iocon_11xx.o ssp_11xx.o sysctl_11xx.o timer_11xx.o
CORE_CHIP_OBJS += uart_11xx.o wwdt_11xx.o
CORE_IP_OBJS = acmp_001.o adc_001.o dac_001.o gpiogrpint_001.o gpiopinint_001.o i2c_001.o ssp_001.o timer_001.o
CORE_IP_OBJS += wwdt_001.o gpio_003.o usart_004.o
ifeq ($(CHIP),CHIP_LPC11CXX)
CORE_IP_OBJS += ccan_001.o
endif
endif

ifeq ($(CHIP_FAMILY),13xx)
ARCH = armv7-m
CPU = cortex-m3
MX = M3
CORE = CORE_$(MX)
CHIP ?= CHIP_LPC1343
CORE_CHIP_OBJS =  adc_13xx.o clock_13xx.o gpio_13xx.o i2c_13xx.o iocon_13xx.o ssp_13xx.o sysctl_13xx.o timer_13xx.o
CORE_CHIP_OBJS += uart_13xx.o wwdt_13xx.o
CORE_IP_OBJS = adc_001.o i2c_001.o ssp_001.o timer_001.o wwdt_001.o usart_004.o
ifeq ($(CHIP),CHIP_LPC1343)
CORE_IP_OBJS += gpio_003.o
endif
ifeq ($(CHIP),CHIP_LPC1347)
FLASHK ?= 64
RAMK ?= 8
CORE_CHIP_OBJS += flash_13xx.o ritimer_13xx.o
CORE_IP_OBJS += gpiogrpint_001.o gpiopinint_001.o ritimer_001.o
endif
FLASHK ?= 32
RAMK ?= 8
endif

ifeq ($(CHIP_FAMILY),17xx_40xx)
ARCH = armv7-m
CPU = cortex-m3
MX = M3
CHIP ?= CHIP_LPC175X_6X
FLASHK ?= 512
RAMK ?= 64
CORE_CHIP_OBJS = adc_17xx_40xx.o can_17xx_40xx.o clock_17xx_40xx.o dac_17xx_40xx.o enet_17xx_40xx.o gpdma_17xx_40xx.o
CORE_CHIP_OBJS += i2c_17xx_40xx.o i2s_17xx_40xx.o iocon_17xx_40xx.o
CORE_CHIP_OBJS += rtc_17xx_40xx.o ssp_17xx_40xx.o sysctl_17xx_40xx.o timer_17xx_40xx.o uart_17xx_40xx.o wwdt_17xx_40xx.o
CORE_IP_OBJS = adc_001.o gpdma_001.o timer_001.o usart_001.o dac_001.o gpio_003.o ssp_001.o rtc_001.o i2c_001.o i2s_001.o
CORE_IP_OBJS += can_001.o enet_002.o wwdt_001.o
ifeq ($(CHIP),CHIP_LPC175X_6X)
CORE_CHIP_OBJS += ritimer_17xx_40xx.o
CORE_IP_OBJS += ritimer_001.o
endif
ifeq ($(CHIP),CHIP_LPC177X_8X)
CORE_CHIP_OBJS += crc_17xx_40xx.o eeprom_17xx_40xx.o emc_17xx_40xx.o lcd_17xx_40xx.o sdc_17xx_40xx.o
CORE_IP_OBJS += crc_001.o eeprom_001.o emc_001.o lcd_001.o sdc_001.o
endif
ifeq ($(CHIP),CHIP_LPC407X_8X)
CORE_CHIP_OBJS += cmp_17xx_40xx.o crc_17xx_40xx.o eeprom_17xx_40xx.o emc_17xx_40xx.o lcd_17xx_40xx.o sdc_17xx_40xx.o
CORE_IP_OBJS += crc_001.o eeprom_001.o emc_001.o lcd_001.o sdc_001.o
endif
endif

CORE_CHIP_DIR = $(LPCOPEN_DIR)/software/lpc_core/lpc_chip/chip_$(CHIP_FAMILY)
CORE_CHIP_COMMON_DIR = $(LPCOPEN_DIR)/software/lpc_core/lpc_chip/chip_common
CORE_IP_DIR = $(LPCOPEN_DIR)/software/lpc_core/lpc_ip
CORE_BOARD_COMMON_DIR = $(LPCOPEN_DIR)/software/lpc_core/lpc_board/board_common

INCLUDES = -I. -I$(LPCOPEN_MAKE_DIR)
INCLUDES += -I$(CMSIS_DIR)/CMSIS/Include
INCLUDES += -I$(LPCOPEN_DIR)/software/lpc_core/lpc_ip
INCLUDES += -I$(CORE_CHIP_COMMON_DIR)
INCLUDES += -I$(CORE_CHIP_DIR)
INCLUDES += -I$(CORE_BOARD_COMMON_DIR)
INCLUDES += $(EXTRA_INCLUDES)

DEFS = -D$(CORE) -D$(CHIP) -D$(ARM_MATH) -D__STACK_SIZE=$(STACK_SIZE) -D__HEAP_SIZE=$(HEAP_SIZE)
DEFS += -DHAVE_FREERTOS=$(HAVE_FREERTOS) -DHAVE_SEMIHOST=$(HAVE_SEMIHOST)
DEFS += $(EXTRA_DEFS)

CFLAGS = $(DEFS) $(INCLUDES)
CFLAGS += -mcpu=$(CPU) -mthumb -march=$(ARCH) #-mthumb-interwork
CFLAGS += $(DEBUG_CFLAGS) $(EXTRA_CFLAGS)

###########################################################################################################
# local objs

OBJS_DIR = build/$(CHIP)/obj

OBJS = $(addprefix $(OBJS_DIR)/, $(patsubst %.cpp,%.o,$(patsubst %.c,%.o,$(SOURCES)))) $(OBJS_DIR)/$(STARTUP).o $(EXTRA_OBJS)

$(OBJS_DIR)/%.o : %.c $(LPCOPEN_MAKEFILES) $(PROG_DEPS)
	$(GCC) -o $@ -c $< $(CFLAGS)

$(OBJS_DIR)/%.o : %.cpp $(LPCOPEN_MAKEFILES) $(PROG_DEPS)
	$(GXX) -o $@ -c $< $(CFLAGS)

$(OBJS_DIR)/%.o : %.S $(LPCOPEN_MAKEFILES) $(PROG_DEPS)
	$(GCC) -o $@ -c $< $(CFLAGS)

###########################################################################################################
# core library

CORE_CHIP_OBJS += $(notdir $(patsubst %.c,%.o,$(wildcard $(CORE_CHIP_COMMON_DIR)/*.c)))

LIBLPCOPENCORE_DIR = build/$(CHIP)/lib/core
LIBLPCOPENCORE_CHIP_DIR = $(LIBLPCOPENCORE_DIR)/chip
LIBLPCOPENCORE_IP_DIR = $(LIBLPCOPENCORE_DIR)/ip

LIBLPCOPENCORE_CHIP_OBJS = $(addprefix $(LIBLPCOPENCORE_CHIP_DIR)/, $(CORE_CHIP_OBJS))
LIBLPCOPENCORE_IP_OBJS = $(addprefix $(LIBLPCOPENCORE_IP_DIR)/, $(CORE_IP_OBJS))
LIBLPCOPENCORE = $(LIBLPCOPENCORE_DIR)/liblpcopencore.a

LPCOPEN_LIBS = $(LIBLPCOPENCORE)

$(LIBLPCOPENCORE_CHIP_DIR)/%.o : $(CORE_CHIP_DIR)/%.c $(LPCOPEN_MAKEFILES)
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBLPCOPENCORE_CHIP_DIR)/%.o : $(CORE_CHIP_COMMON_DIR)/%.c $(LPCOPEN_MAKEFILES)
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBLPCOPENCORE_IP_DIR)/%.o : $(CORE_IP_DIR)/%.c $(LPCOPEN_MAKEFILES)
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBLPCOPENCORE) : $(LIBLPCOPENCORE_CHIP_DIR) $(LIBLPCOPENCORE_CHIP_OBJS) $(LIBLPCOPENCORE_IP_DIR) $(LIBLPCOPENCORE_IP_OBJS)
	$(AR) rv $@ $(LIBLPCOPENCORE_CHIP_OBJS) $(LIBLPCOPENCORE_IP_OBJS)

$(LIBLPCOPENCORE_CHIP_DIR) $(LIBLPCOPENCORE_IP_DIR) $(OBJS_DIR):
	mkdir -p $@

###########################################################################################################
# freeRTOS library

ifeq ($(HAVE_FREERTOS),1)

INCLUDES += -I$(FREERTOS_DIR)/Source/include -I$(FREERTOS_PORTABLE_DIR)
FREERTOS_DIR = $(LPCOPEN_DIR)/software/freertos/freertos
FREERTOS_PORTABLE_DIR = $(FREERTOS_DIR)/Source/portable/GCC/ARM_C$(MX)
FREERTOS_PORTABLE_MEMMANG_DIR = $(FREERTOS_DIR)/Source/portable/MemMang
FREERTOS_HEAP_SRC ?= heap_3.c
FREERTOSLPC_DIR = $(LPCOPEN_DIR)/software/freertos/freertoslpc

FREERTOS_SRCS = $(wildcard $(FREERTOS_DIR)/Source/*.c $(FREERTOS_PORTABLE_DIR)/*.c $(FREERTOSLPC_DIR)/*.c) $(FREERTOS_PORTABLE_MEMMANG_DIR)/$(FREERTOS_HEAP_SRC)
FREERTOS_OBJS = $(notdir $(patsubst %.c,%.o,$(FREERTOS_SRCS)))
LIBFREERTOS_DIR = build/$(CHIP)/lib/freertos
LIBFREERTOS_OBS = $(addprefix $(LIBFREERTOS_DIR)/, $(FREERTOS_OBJS))
LIBFREERTOS = $(LIBFREERTOS_DIR)/libfreertos.a

LPCOPEN_LIBS += $(LIBFREERTOS)

$(LIBFREERTOS_DIR)/%.o : $(FREERTOS_DIR)/Source/%.c $(LPCOPEN_MAKEFILES) FreeRTOSConfig.h
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBFREERTOS_DIR)/%.o : $(FREERTOS_PORTABLE_DIR)/%.c $(LPCOPEN_MAKEFILES) FreeRTOSConfig.h
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBFREERTOS_DIR)/%.o : $(FREERTOS_PORTABLE_MEMMANG_DIR)/%.c $(LPCOPEN_MAKEFILES) FreeRTOSConfig.h
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBFREERTOS_DIR)/%.o : $(FREERTOSLPC_DIR)/%.c $(LPCOPEN_MAKEFILES) FreeRTOSConfig.h
	$(GCC) -o $@ -c $< $(CFLAGS)

$(LIBFREERTOS) : $(LIBFREERTOS_DIR) $(LIBFREERTOS_OBS)
	$(AR) rv $@ $(LIBFREERTOS_OBS)

$(LIBFREERTOS_DIR):
	mkdir -p $@

endif # HAVE_FREERTOS

###########################################################################################################

$(PROG).elf: $(LPCOPEN_DIR) $(OBJS_DIR) $(OBJS) $(LPCOPEN_LIBS) $(LD_SCRIPT)
	$(GCC) -o $@ $(CFLAGS) $(OBJS) $(SPECS) $(EXTRA_LINK_FLAGS) \
	-L$(LPCOPEN_MAKE_DIR) -static $(LIBS) -Xlinker -Map=$(PROG).map -T $(LD_SCRIPT)
	size $@

%.hex : %.elf
	$(OBJCOPY) -O ihex  $< $@

%.bin : %.elf
	$(OBJCOPY) -O binary --pad-to $(BIN_SIZE) $< $@

clean :
	rm -f $(PROG).elf $(PROG).map $(PROG).hex
	rm -rf build/$(CHIP)

realclean :
	rm -rf build

$(LPCOPEN_DIR) :
	@echo
	@echo "Can't find $(LPCOPEN_DIR), either copy or symlink the lpcopen distribution to there."
	@echo
	@false
